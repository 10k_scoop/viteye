import "react-native-gesture-handler";
import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import OnBoarding1 from "../screens/OnBoarding1/OnBoarding1";
import OnBoarding2 from "../screens/OnBoarding2/OnBoarding2";
import OnBoarding3 from "../screens/OnBoarding3/OnBoarding3";
import GetStarted from "../screens/Get-Started/GetStarted";
import Login from "../screens/Login/Login";
import Register1 from "../screens/Register1/Register1";
import Register2 from "../screens/Register2/Register2";

const { Navigator, Screen } = createStackNavigator();

const HomeNavigator = () => (
  <Navigator screenOptions={{ headerShown: false }}>
    <Screen name="OnBoarding1" component={OnBoarding1} />
    <Screen name="OnBoarding2" component={OnBoarding2} />
    <Screen name="OnBoarding3" component={OnBoarding3} />
    <Screen name="GetStarted" component={GetStarted} />
    <Screen name="Login" component={Login} />
    <Screen name="Register1" component={Register1} />
    <Screen name="Register2" component={Register2} />
  </Navigator>
);

export const NoAuthNavigator = () => (
  <NavigationContainer>
    <HomeNavigator />
  </NavigationContainer>
);
