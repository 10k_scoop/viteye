import "react-native-gesture-handler";
import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Subscription from "../screens/Subscription/Subscription";
import MainFirst from "../screens/Main-First/MainFirst";
import Main from "../screens/Main/Main";
import Notification from "../screens/Notifications/Notification";
import AboutInfo from "../screens/About-Info/AboutInfo";
import ScanMole from "../screens/ScanMole/ScanMole";
import ScanExample from "../screens/Scan-Example/ScanExample";
import ScanCheck from "../screens/Scan-Check/ScanCheck";
import Consultation from "../screens/Consultations/Consultation";
import Analyzing from "../screens/Analyzing/Analyzing";
import ScanResult from "../screens/Scan-Result/ScanResult";
import Scans from "../screens/Scans/Scans";
import ScanResult1 from "../screens/Scan-Result1/ScanResult1";
import Chat from "../screens/Chat/Chat";
import DoctorInfo from "../screens/Doctor-Info/DoctorInfo";
import PersonalData from "../screens/Personal-Data/PersonalData";
import EditPersonalData from "../screens/EditPersonal-Data/EditPersonalData";
import Subscription1 from "../screens/Subscription1/Subscription1";
import EducationArticle from "../screens/Education-Article/EducationArticle";
import Articles from "../screens/Articles/Articles";
import About from "../screens/About/About";

const { Navigator, Screen } = createStackNavigator();

const HomeNavigator = () => (
  <Navigator screenOptions={{ headerShown: false }}>
    <Screen name="MainFirst" component={MainFirst} />
    <Screen name="Main" component={Main} />
    <Screen name="Subscription" component={Subscription} />
    <Screen name="Notification" component={Notification} />
    <Screen name="AboutInfo" component={AboutInfo} />
    <Screen name="ScanMole" component={ScanMole} />
    <Screen name="ScanExample" component={ScanExample} />
    <Screen name="ScanCheck" component={ScanCheck} />
    <Screen name="Consultation" component={Consultation} />
    <Screen name="Analyzing" component={Analyzing} />
    <Screen name="ScanResult" component={ScanResult} />
    <Screen name="Scans" component={Scans} />
    <Screen name="ScanResult1" component={ScanResult1} />
    <Screen name="Chat" component={Chat} />
    <Screen name="DoctorInfo" component={DoctorInfo} />
    <Screen name="PersonalData" component={PersonalData} />
    <Screen name="EditPersonalData" component={EditPersonalData} />
    <Screen name="Subscription1" component={Subscription1} />
    <Screen name="EducationArticle" component={EducationArticle} />
    <Screen name="Articles" component={Articles} />
    <Screen name="About" component={About} />
  </Navigator>
);

export const AppNavigator = () => (
  <NavigationContainer>
    <HomeNavigator />
  </NavigationContainer>
);
