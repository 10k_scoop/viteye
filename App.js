import React, { useState, useEffect } from "react";
import { AppNavigator } from "./routes/AppNavigator";
import { Provider } from "react-redux";
import store from "./state-management/store";
import * as firebase from "firebase";
import { firebaseConfig } from "./state-management/actions/env";
import { NoAuthNavigator } from "./routes/NoAuthNavigator";
export default function App() {
  const [role, setRole] = useState("loggedOut");
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    try {
      if (firebase.apps.length === 0) {
        firebase.initializeApp(firebaseConfig);
      }
    } catch (error) {
      alert("Check you internet connection!!");
    }
    firebase.auth().onAuthStateChanged(function (user) {
      if (user) {
        var db = firebase.firestore();
        setLoading(true);
        db.collection("users")
          .where("email", "==", user.providerData[0].email)
          .get()
          .then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
              setLoading(false);
              setRole("LoggedIn");
            });
          })
          .catch((error) => {
            console.log("Error getting documents: ", error);
            setLoading(false);
          });
      } else {
        setRole("LoggedOut");
        setLoading(false);
      }
    });
  }, []);

  return (
    <Provider store={store}>
      {role == "LoggedIn" ? <AppNavigator /> : <NoAuthNavigator />}
    </Provider>
  );
}
