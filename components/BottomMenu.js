import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { FontAwesome, Ionicons, AntDesign, Entypo } from "@expo/vector-icons";

export default function BottomMenu(props) {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.IconView}
        onPress={() => props.navigation.navigate("Main")}
      >
        <Ionicons name="md-home-sharp" size={rf(22)} color="#007AFF" />
        <Text style={{ fontSize: rf(12), color: "#007AFF" }}>Home</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.IconView}
        onPress={() => props.navigation.navigate("EducationArticle")}
      >
        <Ionicons name="chatbubble-sharp" size={rf(22)} color="grey" />
        <Text style={{ fontSize: rf(12), color: "grey" }}>blogs</Text>
      </TouchableOpacity>
      <View style={styles.Rapper}>
        <TouchableOpacity
          style={styles.ScanIcon}
          onPress={() => props.navigation.navigate("ScanMole")}
        >
          <AntDesign name="scan1" size={rf(24)} color="#fff" />
        </TouchableOpacity>
      </View>
      <TouchableOpacity
        style={styles.IconView}
        onPress={() => props.navigation.navigate("Scans")}
      >
        <FontAwesome name="search" size={rf(22)} color="grey" />
        <Text style={{ fontSize: rf(12), color: "grey" }}>Scans</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.IconView}
        onPress={() => props.navigation.navigate("AboutInfo")}
      >
        <Entypo name="menu" size={rf(22)} color="grey" />
        <Text style={{ fontSize: rf(12), color: "grey" }}>More</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("8%"),
    backgroundColor: "#F6F6F7",
    position: "absolute",
    bottom: 0,
    flexDirection: "row",
    borderTopWidth: 1,
    borderTopColor: "#e5e5e5",
  },
  IconView: {
    width: "20%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  ScanIcon: {
    width: hp("7%"),
    height: hp("7%"),
    borderRadius: 100,
    backgroundColor: "#007AFF",
    justifyContent: "center",
    alignItems: "center",
  },
  Rapper: {
    width: "20%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
});
