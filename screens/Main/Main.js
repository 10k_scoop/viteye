import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import BottomMenu from "../../components/BottomMenu";
import { ScrollView } from "react-native-gesture-handler";
import Header from "./components/Header";
import Cards from "./components/Cards";

export default function Main({ navigation }) {
  return (
    <View style={styles.container}>
      <Header onPress={() => navigation.navigate("Notification")} />
      <ScrollView>
        <View
          style={{
            width: wp("100%"),
            alignItems: "center",
            marginBottom: hp("9%"),
          }}
        >
          <View style={styles.title}>
            <Text style={{ fontSize: rf(14), color: "grey" }}>
              Researched moles
            </Text>
          </View>

          {/* Discription */}
          <View style={styles.discriptionsWrapper}>
            <View style={styles.Inner1}>
              <Text style={styles.Font1}>Days from the last screening</Text>
              <Text style={styles.Font2}>18</Text>
            </View>
            <View style={styles.Inner2}>
              <Text style={styles.Font1}>Total Scans</Text>
              <Text style={styles.Font2}>122</Text>
            </View>
          </View>
          <View style={styles.discriptionsWrapper2}>
            <View style={styles.Wrapper2Inner1}>
              <Text style={styles.Font1}>Melanoma detected</Text>
              <Text style={styles.Font2}>5</Text>
            </View>
            <View style={styles.Wrapper2Inner2}></View>
          </View>
          {/* Discriptions */}
          <View style={styles.Secondtitle}>
            <Text style={{ fontSize: rf(14), color: "grey" }}>
              Recomended consultations
            </Text>
          </View>
          {/* Cards */}
          <Cards />
          <Cards />
          <Cards />
          <Cards />
          <Cards />
          <Cards />
          {/* Cards */}
        </View>
      </ScrollView>
      <BottomMenu navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  title: {
    width: wp("100%"),
    height: hp("7%"),
    borderBottomWidth: 1,
    borderBottomColor: "#e5e5e5",
    justifyContent: "flex-end",
    paddingVertical: 10,
    paddingHorizontal: 16,
    marginBottom: 10,
  },
  discriptionsWrapper: {
    width: wp("90%"),
    height: hp("15%"),
    flexDirection: "row",
    alignItems: "flex-end",
    borderBottomWidth: 1,
    borderColor: "#e5e5e5",
  },
  Inner1: {
    width: "50%",
    height: "90%",
    borderRightWidth: 1,
    borderColor: "#e5e5e5",
    justifyContent: "space-evenly",
  },
  Inner2: {
    width: "50%",
    height: "80%",
    paddingHorizontal: 12,
    justifyContent: "space-around",
  },
  discriptionsWrapper2: {
    width: wp("90%"),
    height: hp("15%"),
    flexDirection: "row",
  },
  Wrapper2Inner1: {
    width: "50%",
    height: "90%",
    borderRightWidth: 1,
    borderColor: "#e5e5e5",
    justifyContent: "space-around",
  },
  Wrapper2Inner2: {
    width: "50%",
    height: "80%",
    paddingHorizontal: 12,
    justifyContent: "space-around",
  },
  Font1: {
    fontSize: rf(18),
    color: "grey",
  },
  Font2: {
    fontSize: rf(20),
    fontWeight: "700",
  },
  Secondtitle: {
    width: wp("100%"),
    height: hp("4%"),
    borderBottomWidth: 1,
    borderBottomColor: "#e5e5e5",
    justifyContent: "flex-end",
    paddingVertical: 10,
    paddingHorizontal: 18,
    marginBottom: 10,
  },
});
