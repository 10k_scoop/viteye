import React from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { heightPercentageToDP as hp, widthPercentageToDP as wp, } from "react-native-responsive-screen";
import { FontAwesome } from '@expo/vector-icons';

export default function Header({onPress}) {

    return (
        <View style={styles.container}>
            <View style={styles.main}>
                <Text style={{ fontSize: rf(12), color: "grey" }}>MON, 26 SEP</Text>
                <View style={styles.Inner}>
                    <Text style={{ fontSize: rf(26), fontWeight: "700" }}>Lilah Grace</Text>
                    <View style={styles.Icons}>
                        <View style={styles.Img}>
                            <Image 
                            source={require('../../../assets/profile.jpg')}
                            style={{width:"100%",height:"100%"}}
                            />
                        </View>
                        <TouchableOpacity onPress={onPress}>
                        <FontAwesome name="bell" size={rf(22)} color="#007AFF" />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>

        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        width: wp('100%'),
        height: hp('18%'),
        backgroundColor: "#F8F8F8",
        justifyContent: "flex-end",
    },
    main: {
        width: "100%",
        height: "50%",
        paddingHorizontal: "5%",
    },
    Inner: {
        width: "100%",
        height: "60%",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },
    Icons: {
        width: "20%",
        height: "100%",
        flexDirection: "row",
        justifyContent:"space-evenly",
        alignItems:"center"
    },
    Img: {
        width: hp('4%'),
        height: hp('4%'),
        borderRadius: 100,
        overflow:"hidden"
    }


});
