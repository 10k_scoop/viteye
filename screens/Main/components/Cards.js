import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { heightPercentageToDP as hp, widthPercentageToDP as wp, } from "react-native-responsive-screen";

export default function Cards() {

    return (
        <TouchableOpacity style={styles.container}>
            <View style={styles.InnerRow}>
                <Text style={{fontSize:rf(14),color:"grey"}}>Lilah Grace</Text>
                <Text style={{fontSize:rf(15),color:"grey"}}>YESTERDAY</Text>
            </View>
            <Text style={{fontSize:rf(17),color:"#222",fontWeight:"700"}}>Right shoulder mole</Text>
            <Text style={{fontSize:rf(14),color:"#007AFF"}}>Melanoma probability 97%</Text>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        width: wp('95%'),
        height: hp('13%'),
        backgroundColor: "#F2F2F7",
        borderRadius: 10,
        paddingHorizontal:10,
        justifyContent:"space-evenly",
        paddingVertical:5,
        marginBottom:10
    },
    InnerRow: {
        width: "100%",
        height: "30%",
        flexDirection: "row",
        justifyContent:"space-between",
        alignItems:"center",

    }


});
