import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "./components/Header";

export default function Subscription1({ navigation }) {
  return (
    <View style={styles.container}>
      <Header onPress={() => navigation.goBack()} />
      <View style={styles.CircleRow}>
        <View style={styles.Circle}></View>
      </View>
      <View style={styles.heading}>
        <Text style={{ fontSize: rf(20), textAlign: "center" }}>
          Your subscription is active till{"\n"} 23.03.2022
        </Text>
      </View>
      <View style={styles.discription}>
        <Text style={{ fontSize: rf(14), color: "grey", textAlign: "center" }}>
          The subscription will automatically renew{"\n"} until you stop it. You
          can terminate your{"\n"} subscription at any time.
        </Text>
      </View>
      <TouchableOpacity style={styles.btn}>
        <Text style={{ fontSize: rf(14), color: "#007AFF", fontWeight: "700" }}>
          Stop subscription
        </Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  CircleRow: {
    width: wp("100%"),
    height: hp("40%"),
    justifyContent: "center",
    alignItems: "center",
  },
  Circle: {
    width: hp("28%"),
    height: hp("28%"),
    borderRadius: 100,
    backgroundColor: "#DDEDFF",
  },
  heading: {
    width: wp("100%"),
    height: hp("8%"),
    alignItems: "center",
    justifyContent: "center",
  },
  discription: {
    width: wp("100%"),
    height: hp("12%"),
    justifyContent: "center",
    marginBottom: hp("10%"),
  },
  btn: {
    width: wp("90%"),
    height: hp("7%"),
    borderRadius: 10,
    backgroundColor: "#E9E9EB",
    justifyContent: "center",
    alignItems: "center",
  },
});
