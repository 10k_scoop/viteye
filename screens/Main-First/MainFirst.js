import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "./components/Header";
import BottomMenu from "../../components/BottomMenu";
import Cards from "./components/Cards";
import { ScrollView } from "react-native-gesture-handler";
import { getUserData } from "../../state-management/actions/auth/FirebaseAuthActions";
import { connect } from "react-redux";
import * as firebase from "firebase";

const MainFirst = (props) => {
  const [userData, setUserData] = useState();
  const user = firebase.auth().currentUser;
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    props.getUserData(user.uid, setLoading);
  }, []);

  useEffect(() => {
    setUserData(props.get_user_details);
  }, [props]);

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator size="large" color="#222" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Header profile={userData?.profile} />
      <ScrollView>
        <View
          style={{
            width: wp("100%"),
            alignItems: "center",
            marginBottom: hp("9%"),
          }}
        >
          <View style={styles.CircleRow}>
            <View style={styles.Circle}></View>
          </View>
          <Text style={{ fontSize: rf(20) }}>Scanning info is not found</Text>
          <TouchableOpacity
            style={styles.btn}
            onPress={() => props.navigation.navigate("ScanMole")}
          >
            <Text style={{ fontSize: rf(16), color: "#fff" }}>
              Make a fist scan
            </Text>
          </TouchableOpacity>
          <View style={styles.Discription}>
            <Text style={{ fontSize: rf(14), color: "grey" }}>
              Education materials
            </Text>
          </View>
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            <View style={styles.cardsWrapper}>
              <Cards />
              <Cards />
              <Cards />
              <Cards />
              <Cards />
              <Cards />
              <Cards />
            </View>
          </ScrollView>
        </View>
      </ScrollView>
      <BottomMenu navigation={props.navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  CircleRow: {
    width: wp("100%"),
    height: hp("40%"),
    justifyContent: "center",
    alignItems: "center",
  },
  Circle: {
    width: hp("30%"),
    height: hp("30%"),
    borderRadius: hp("30%"),
    backgroundColor: "#DDEDFF",
  },
  btn: {
    width: wp("90%"),
    height: hp("7%"),
    backgroundColor: "#007AFF",
    borderRadius: 10,
    marginTop: "10%",
    justifyContent: "center",
    alignItems: "center",
  },
  Discription: {
    width: wp("100%"),
    height: hp("12%"),
    borderBottomWidth: 1,
    borderBottomColor: "#e5e5e5",
    justifyContent: "flex-end",
    paddingVertical: "2%",
    paddingHorizontal: "6%",
  },
  cardsWrapper: {
    flexDirection: "row",
    marginRight: 10,
    alignItems: "center",
    justifyContent: "center",
    marginTop: hp("2%"),
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details: state.main.get_user_details,
});
export default connect(mapStateToProps, { getUserData })(MainFirst);
