import React, { useState } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default function Cards({ navigation }) {
  return (
    <TouchableOpacity style={styles.container}>
      <Text style={{ fontSize: rf(16), fontWeight: "700" }}>Article Title</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("45%"),
    height: hp("35%"),
    backgroundColor: "#F2F2F7",
    borderRadius: 10,
    marginLeft: 10,
    justifyContent: "flex-end",
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
});
