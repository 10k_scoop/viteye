import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default function StopBtn() {
  return (
    <TouchableOpacity style={styles.container}>
      <Text style={{ fontSize: rf(14), fontWeight: "700" }}>
        Stop Consultations
      </Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("90%"),
    height: hp("7%"),
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F4F4F5",
  },
});
