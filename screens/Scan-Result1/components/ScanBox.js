import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default function ScanBox(props) {
  return <View style={[styles.Box, { backgroundColor: props.color }]}></View>;
}

const styles = StyleSheet.create({
  Box: {
    width: wp("15%"),
    height: hp("8%"),
    borderRadius: 5,
    marginRight: wp("2%"),
  },
});
