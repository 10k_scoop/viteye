import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default function DataCard(props) {
  return (
    <View style={styles.container}>
      <View style={styles.Inner}>
        <Text style={{ color: "grey", fontSize: rf(12) }}>{props.txt}</Text>
      </View>
      <View style={styles.Inner2}>
        <Text style={{ fontSize: rf(18) }}>{props.txt2}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("10%"),
    justifyContent: "center",
    alignItems: "flex-end",
  },
  Inner: {
    width: "95%",
    height: "30%",
    borderBottomWidth: 1,
    borderBottomColor: "#e5e5e5",
  },
  Inner2: {
    width: "95%",
    height: "50%",
    justifyContent: "center",
  },
});
