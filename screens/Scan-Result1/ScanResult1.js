import React, { useState } from "react";
import { StyleSheet, Text, View, Alert, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { AntDesign } from "@expo/vector-icons";
import DataCard from "./components/DataCard";
import StopBtn from "./components/StopBtn";
import ScanBox from "./components/ScanBox";
import ChatBtn from "./components/ChatBtn";
import Header from "./components/Header";

export default function ScanResult1({ navigation }) {
  return (
    <View style={styles.container}>
      <Header onclick={() => navigation.goBack()} />
      <View style={styles.result}>
        <Text style={{ fontSize: rf(28), fontWeight: "700", color: "#007AFF" }}>
          97%
        </Text>
        <Text style={{ fontSize: rf(14) }}>Melanoma probability</Text>
      </View>
      {/* DataCards */}
      <DataCard txt="Tested Patient Name" txt2="Dovgopol Dmitriy" />
      {/* DataCards */}
      <View style={styles.AnalisisRow}>
        <View style={styles.InnerAnalisis}>
          <Text style={{ fontSize: rf(14), color: "grey" }}>Analisis scan</Text>
        </View>
      </View>
      <View style={styles.ScanBoxRow}>
        <ScanBox color="#DDEDFF" />
        <ScanBox color="#D2E7FF" />
        <ScanBox color="#B9DAFF" />
        <ScanBox color="#8CC2FF" />
        <TouchableOpacity style={styles.PlusBtn}>
          <AntDesign name="plus" size={rf(26)} color="#e5e5e5" />
        </TouchableOpacity>
      </View>

      {/* Buttons */}
      <ChatBtn onClick={() => navigation.navigate("Chat")} />
      <StopBtn />
      {/* Buttons */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  result: {
    width: wp("100%"),
    height: hp("15%"),
    justifyContent: "center",
    alignItems: "center",
  },
  AnalisisRow: {
    width: wp("100%"),
    height: hp("6%"),
    alignItems: "flex-end",
    justifyContent: "center",
  },
  InnerAnalisis: {
    width: "95%",
    height: "60%",
    borderBottomWidth: 1,
    borderBottomColor: "#e5e5e5",
  },
  ScanBoxRow: {
    width: wp("90%"),
    height: hp("10%"),
    flexDirection: "row",
    alignItems: "center",
    marginBottom: hp("8%"),
  },
  PlusBtn: {
    width: wp("15%"),
    height: hp("8%"),
    borderWidth: 1,
    borderRadius: 5,
    borderColor: "#e5e5e5",
    justifyContent: "center",
    alignItems: "center",
  },
});
