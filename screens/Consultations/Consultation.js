import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default function Consultation({ navigation }) {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={{ fontSize: rf(18), fontWeight: "700" }}>Mole title</Text>
      </View>
      <View style={styles.title}>
        <Text style={{ fontSize: rf(18) }}>Type mole\analisis title</Text>
      </View>
      <View style={styles.discription}>
        <Text style={{ fontSize: rf(14) }}>Right hand mole#2</Text>
      </View>

      <View style={styles.BtnRow}>
        <TouchableOpacity
          style={styles.ContinueBtn}
          onPress={() => navigation.navigate("ScanResult")}
        >
          <Text style={{ fontSize: rf(16), color: "#fff" }}>Continue</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  header: {
    width: wp("100%"),
    height: hp("10%"),
    justifyContent: "flex-end",
    alignItems: "center",
  },
  title: {
    width: wp("100%"),
    height: hp("15%"),
    justifyContent: "center",
    alignItems: "center",
  },
  discription: {
    width: wp("90%"),
    height: hp("10%"),
    borderBottomWidth: 1,
    borderBottomColor: "#e5e5e5",
    justifyContent: "flex-end",
    paddingVertical: 10,
  },
  ContinueBtn: {
    width: wp("90%"),
    height: hp("7%"),
    backgroundColor: "#007AFF",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 8,
  },
  BtnRow: {
    width: wp("100%"),
    height: hp("60%"),
    justifyContent: "flex-end",
    alignItems: "center",
  },
});
