import React, { useState } from "react";
import { StyleSheet, Text, View, TextInput } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default function TextField(props) {
  return (
    <View style={styles.container}>
      <Text style={{ fontSize: rf(14), color: "grey" }}>{props.title}</Text>
      <TextInput
        style={styles.TxtField}
        placeholder={props.txtfield}
        placeholderTextColor="#222"
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("90%"),
    height: hp("8%"),
    justifyContent: "space-evenly",
    marginBottom: 5,
  },
  TxtField: {
    width: "100%",
    height: "70%",
    fontWeight: "700",
    fontSize: rf(16),
  },
});
