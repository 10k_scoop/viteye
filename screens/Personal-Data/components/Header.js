import React, { useState } from "react";
import { StyleSheet, Text, View, Alert, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { AntDesign } from "@expo/vector-icons";

export default function Header(props) {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={props.onClick}>
        <AntDesign name="left" size={rf(22)} color="#007AFF" />
      </TouchableOpacity>
      <Text style={{ fontSize: rf(16), fontWeight: "700" }}>Personal data</Text>
      <TouchableOpacity onPress={props.onEditClick}>
        <Text style={{ fontSize: rf(16), color: "#007AFF" }}>Edit</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("10%"),
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "space-between",
    paddingHorizontal: 10,
  },
});
