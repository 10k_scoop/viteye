import React, { useState } from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default function Profile(props) {
  return (
    <View style={styles.container}>
      <View style={styles.img}>
        <Image
          source={
            props.profile
              ? { uri: props.profile }
              : require("../../../assets/PersonalPic.png")
          }
          style={{ width: "100%", height: "100%" }}
          resizeMode="cover"
        />
      </View>
      <Text style={{ fontSize: rf(24), fontWeight: "700" }}>{props.name}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("27%"),
    justifyContent: "center",
    alignItems: "center",
  },
  img: {
    width: hp("10%"),
    height: hp("10%"),
    borderRadius: 100,
    marginBottom: 10,
    backgroundColor: "#C5F0C6",
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 10,
  },
});
