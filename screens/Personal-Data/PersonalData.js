import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { AntDesign, Feather } from "@expo/vector-icons";
import Header from "./components/Header";
import Profile from "./components/Profile";
import TextField from "./components/TextField";
import { connect } from "react-redux";

const PersonalData = (props) => {
  return (
    <View style={styles.container}>
      <Header
        onClick={() => props.navigation.goBack()}
        onEditClick={() => props.navigation.navigate("EditPersonalData")}
      />
      {/* Profile */}
      <Profile
        name={props.get_user_details?.name}
        profile={props.get_user_details?.profile}
      />
      {/* Profile */}

      <ScrollView showsVerticalScrollIndicator={false}>
        <KeyboardAvoidingView behavior="padding">
          <View style={{ width: wp("100%"), alignItems: "center" }}>
            <View style={styles.MedicalData}>
              <View style={styles.Inner}>
                <Text style={{ fontSize: rf(14) }}>Medical data</Text>
                <TouchableOpacity style={{ marginRight: 8 }}>
                  <AntDesign name="right" size={rf(20)} color="grey" />
                </TouchableOpacity>
              </View>
            </View>
            {/* TextFields */}
            <TextField title="Email" txtfield={props.get_user_details?.email} />
            <TextField
              title="Age"
              txtfield={props?.get_user_details?.age || "none"}
            />
            <TextField
              title="Biological gender"
              txtfield={props.get_user_details?.gender || "none"}
            />
            {/* TextFields */}
            <View style={styles.SkinColor}>
              <Text style={styles.Font}>Skin color</Text>
            </View>
            <View style={styles.ColorsRow}>
              <View style={styles.ColorBox}></View>
            </View>
            <View style={styles.Subscription}>
              <Text style={styles.Font}>Subscription</Text>
            </View>
            <View style={styles.Active}>
              <Text style={{ fontSize: rf(16), fontWeight: "700" }}>
                Active til 12.12.2022
              </Text>
              <TouchableOpacity>
                <Feather name="alert-circle" size={rf(18)} color="#007AFF" />
              </TouchableOpacity>
            </View>
            <View style={styles.DeleteAccount}>
              <TouchableOpacity>
                <Text style={{ fontSize: rf(16), color: "red" }}>
                  Delete Account
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  MedicalData: {
    width: wp("100%"),
    height: hp("6%"),
    alignItems: "flex-end",
  },
  Inner: {
    width: "95%",
    height: "80%",
    borderBottomWidth: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    borderBottomColor: "#e5e5e5",
  },
  SkinColor: {
    width: wp("90%"),
    height: hp("5%"),
    justifyContent: "center",
  },
  ColorsRow: {
    width: wp("90%"),
    height: hp("8%"),
    justifyContent: "center",
  },
  ColorBox: {
    width: "15%",
    height: "90%",
    backgroundColor: "#8CC2FF",
    borderRadius: 10,
  },
  Subscription: {
    width: wp("90%"),
    height: hp("5%"),
    justifyContent: "center",
  },
  Font: {
    fontSize: rf(14),
    color: "grey",
  },
  Active: {
    width: wp("90%"),
    height: hp("6%"),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  DeleteAccount: {
    width: wp("100%"),
    height: hp("5%"),
    alignItems: "center",
    justifyContent: "flex-end",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details: state.main.get_user_details,
});
export default connect(mapStateToProps, {})(PersonalData);
