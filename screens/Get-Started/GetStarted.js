import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { AntDesign } from "@expo/vector-icons";

export default function GetStarted({ navigation }) {
  return (
    <View style={styles.container}>
      <View style={styles.CircleRow}>
        <View style={styles.Circle}></View>
      </View>
      <View style={styles.title}>
        <Text style={{ fontSize: rf(24), fontWeight: "700" }}>
          Get Started!
        </Text>
      </View>

      <TouchableOpacity
        style={styles.Btn}
        onPress={() => navigation.navigate("Register1")}
      >
        <Text style={{ fontSize: rf(16), color: "#fff", fontWeight: "700" }}>
          Register
        </Text>
      </TouchableOpacity>
      <View style={styles.Login}>
        <TouchableOpacity onPress={() => navigation.navigate("Login")}>
          <Text style={{ fontSize: 16, color: "#007AFF" }}>Log in</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  CircleRow: {
    width: wp("100%"),
    height: hp("50%"),
    justifyContent: "flex-end",
    alignItems: "center",
  },
  Circle: {
    width: wp("65%"),
    height: wp("65%"),
    borderRadius: wp("65%"),
    backgroundColor: "#DDEDFF",
  },
  title: {
    width: wp("100%"),
    height: hp("20%"),
    justifyContent: "center",
    alignItems: "center",
  },
  Btn: {
    width: wp("90%"),
    height: hp("7%"),
    backgroundColor: "#007AFF",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
  },
  Login: {
    width: wp("100%"),
    height: hp("10%"),
    alignItems: "center",
    justifyContent: "center",
  },
});
