import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import BottomMenu from "../../components/BottomMenu";
import { ScrollView } from "react-native-gesture-handler";
import Header from "./components/Header";
import NotificationsCard from "./components/NotoficationsCard";

export default function Notification({ navigation }) {
  return (
    <View style={styles.container}>
      <Header onPress={() => navigation.goBack()} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Wrapper}></View>

        <NotificationsCard
          title="MOLE RESEARCH"
          discription="You will need to make a scan of the mole on the left hand tomorrow."
        />
        <NotificationsCard
          title="CONSULTATION"
          discription="A consultation about the mole is scheduled for the 23 of March."
        />
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Wrapper: {
    width: wp("90%"),
    alignItems: "center",
    marginTop: hp("5%"),
  },
});
