import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default function NotificationsCard({ title, discription }) {
  return (
    <TouchableOpacity style={styles.container}>
      <View style={styles.InnerRow}>
        <View style={styles.WrapperIconTxt}>
          <View style={styles.Icon}></View>

          <Text style={{ fontSize: rf(14), color: "grey" }}>{title}</Text>
        </View>
        <Text style={{ fontSize: rf(15), color: "grey" }}>1h ago</Text>
      </View>
      <Text style={{ fontSize: rf(16), color: "#222", fontWeight: "600" }}>
        {discription}
      </Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("95%"),
    height: hp("13%"),
    backgroundColor: "#F2F2F7",
    borderRadius: 10,
    paddingHorizontal: 10,
    justifyContent: "space-evenly",
    paddingVertical: 5,
    marginBottom: 10,
  },
  InnerRow: {
    width: "100%",
    height: "30%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  Icon: {
    width: "15%",
    height: "80%",
    borderRadius: 5,
    backgroundColor: "#007AFE",
    marginRight: 8,
  },
  WrapperIconTxt: {
    width: "40%",
    height: "100%",
    flexDirection: "row",
    alignItems: "center",
  },
});
