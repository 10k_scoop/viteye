import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { AntDesign } from "@expo/vector-icons";
export default function Header({ onPress }) {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={onPress}>
        <AntDesign name="left" size={rf(22)} color="#007AFE" />
      </TouchableOpacity>
      <View style={styles.heading}>
        <Text style={{ fontSize: rf(18), fontWeight: "700" }}>
          Scan Example
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("12%"),
    flexDirection: "row",
    alignItems: "flex-end",
    paddingVertical: 10,
    paddingHorizontal: 3,
  },
  heading: {
    width: "90%",
    alignItems: "center",
  },
});
