import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "./components/Header";

export default function ScanExample({ navigation }) {
  return (
    <View style={styles.container}>
      <Header onPress={() => navigation.goBack()} />
      <View style={styles.Img}>
        <ImageBackground
          style={{ width: "100%", height: "100%" }}
          source={require("../../assets/Example.png")}
        >
          <View style={styles.discription}>
            <Text
              style={{ fontSize: rf(16), color: "#fff", textAlign: "center" }}
            >
              Take a photo of an analyzed mole{"\n"}
              <Text style={{ fontWeight: "700" }}> with a dermatoscope.</Text>
            </Text>
          </View>
          <View style={styles.WarningScan}>
            <Text
              style={{ fontSize: rf(16), fontWeight: "700", color: "#fff" }}
            >
              You should make 2 scans
            </Text>
          </View>
        </ImageBackground>
      </View>
      <TouchableOpacity
        style={styles.Btn}
        onPress={() => navigation.navigate("ScanCheck")}
      >
        <Text style={{ fontSize: rf(16), fontWeight: "700", color: "#fff" }}>
          Ok
        </Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Img: {
    width: wp("100%"),
    height: hp("60%"),
    marginTop: "10%",
    marginBottom: hp("10%"),
  },
  discription: {
    width: "100%",
    height: "20%",
    justifyContent: "center",
    alignItems: "center",
  },
  WarningScan: {
    width: "100%",
    height: "70%",
    justifyContent: "flex-end",
    alignItems: "center",
  },
  Btn: {
    width: wp("90%"),
    height: hp("7%"),
    backgroundColor: "#007AFF",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
  },
});
