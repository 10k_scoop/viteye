import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default function Analyzing({ navigation }) {
  return (
    <View style={styles.container}>
      <View style={styles.title}>
        <Text style={{ fontSize: rf(18), fontWeight: "700" }}>
          Analyzing...
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },

  title: {
    width: wp("100%"),
    height: hp("20%"),
    justifyContent: "flex-end",
    alignItems: "center",
  },
});
