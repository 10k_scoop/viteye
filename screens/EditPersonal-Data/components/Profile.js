import React, { useState } from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { FontAwesome5 } from "@expo/vector-icons";

export default function Profile(props) {
  return (
    <View style={styles.container}>
      <View style={styles.imgWrapper}>
        <View style={styles.img}>
          <Image
            source={
              props.profile
                ? { uri: props.profile }
                : require("../../../assets/PersonalPic.png")
            }
            style={{ width: "100%", height: "100%" }}
            resizeMode="cover"
          />
        </View>
        <TouchableOpacity style={styles.EditIcon} onPress={props.onPenPress}>
          <FontAwesome5 name="pen" size={rf(13)} color="#fff" />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("27%"),
    justifyContent: "center",
    alignItems: "center",
  },
  imgWrapper: {
    width: hp("15%"),
    height: hp("15%"),
  },
  img: {
    width: hp("15%"),
    height: hp("15%"),
    borderRadius: 100,
    marginBottom: 10,
    backgroundColor: "#C5F0C6",
    justifyContent: "center",
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 10,
    overflow: "hidden",
  },
  EditIcon: {
    width: hp("4%"),
    height: hp("4%"),
    borderRadius: 100,
    position: "absolute",
    top: 3,
    right: 5,
    backgroundColor: "#007AFF",
    justifyContent: "center",
    alignItems: "center",
  },
});
