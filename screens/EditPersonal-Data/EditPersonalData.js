import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  KeyboardAvoidingView,
  ActivityIndicator,
  Platform,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { FontAwesome5 } from "@expo/vector-icons";
import Header from "./components/Header";
import Profile from "./components/Profile";
import TextField from "./components/TextField";
import { connect } from "react-redux";
import * as ImagePicker from "expo-image-picker";
import * as firebase from "firebase";
import { updateProfile } from "../../state-management/actions/Features/Actions";
import { getUserData } from "../../state-management/actions/auth/FirebaseAuthActions";
const EditPersonalData = (props) => {
  const [image, setImage] = useState(null);
  const [name, setName] = useState(props?.get_user_details?.name);
  const [age, setAge] = useState(props?.get_user_details?.age);
  const [gender, setGender] = useState(props?.get_user_details?.gender);
  const [skinColor, setSkinColor] = useState("");
  const [loading, setLoading] = useState(false);
  const user = firebase.auth().currentUser;

  useEffect(() => {
    (async () => {
      if (Platform.OS !== "web") {
        const { status } =
          await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== "granted") {
          alert("Sorry, we need camera roll permissions to make this work!");
        }
      }
    })();
  }, []);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.All,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setImage(result.uri);
    }
  };

  const onUpdate = () => {
    setLoading(true);
    let uploadUri =
      Platform.OS === "ios" ? image.replace("file://", "") : image;
    let data = {
      name: name,
      gender: gender,
      profile: uploadUri,
      age: age,
    };
    props.updateProfile(data, user.uid, setLoading);
  };

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator size="large" color="orange" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Header
        onClick={() => props.navigation.goBack()}
        onDoneClick={() => onUpdate()}
      />
      <Profile
        name={props.get_user_details?.name}
        profile={image ? image : props.get_user_details?.profile}
        onPenPress={() => pickImage()}
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        <TextField
          title="Name"
          value={name}
          onChangeText={(val) => setName(val)}
        />
        <TextField title="Email" value={props?.get_user_details?.email} />
        <TextField
          title="Age"
          value={age}
          onChangeText={(val) => setAge(val)}
        />
        <TextField
          title="Biological gender"
          value={gender}
          onChangeText={(val) => setGender(val)}
        />
        <View style={styles.SkinColor}>
          <Text style={styles.Font}>Skin color</Text>
        </View>
        <View style={styles.ColorsRow}>
          <View style={styles.ColorBox}>
            <TouchableOpacity>
              <FontAwesome5 name="pen" size={rf(18)} color="#222" />
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  SkinColor: {
    width: wp("90%"),
    height: hp("5%"),
    justifyContent: "center",
  },
  ColorsRow: {
    width: wp("90%"),
    height: hp("8%"),
    justifyContent: "center",
  },
  ColorBox: {
    width: "15%",
    height: "70%",
    backgroundColor: "#8CC2FF",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
  },
  Font: {
    fontSize: rf(14),
    color: "grey",
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details: state.main.get_user_details,
});
export default connect(mapStateToProps, { updateProfile, getUserData })(
  EditPersonalData
);
