import React, { useState } from "react";
import { StyleSheet, Text, View, Alert } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "./components/Header";
import DataCards from "./components/DataCards";
import ScanBox from "./components/ScanBox";
import ActiveBtn from "./components/ActiveBtn";
import MainMenuBtn from "./components/MainMenuBtn";

export default function ScanResult({ navigation }) {
  const onActivate = () => {
    Alert.alert(
      "Activate",
      "My Alert Msg",
      [
        { text: "Activate", onPress: () => console.log("OK Pressed") },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
      ],
      { cancelable: false }
    );
  };

  return (
    <View style={styles.container}>
      <Header />
      <View style={styles.result}>
        <Text style={{ fontSize: rf(28), fontWeight: "700", color: "#007AFF" }}>
          97%
        </Text>
        <Text style={{ fontSize: rf(14) }}>Melanoma probability</Text>
      </View>
      {/* DataCards */}
      <DataCards txt="Tested Patient Name" txt2="Dovgopol Dmitriy" />
      <DataCards txt="Tested skin zone" txt2="Right hand mole#2" />
      {/* DataCards */}
      <View style={styles.AnalisisRow}>
        <View style={styles.InnerAnalisis}>
          <Text style={{ fontSize: rf(14), color: "grey" }}>Analisis scan</Text>
        </View>
      </View>
      <View style={styles.ScanBoxRow}>
        <ScanBox color="#DDEDFF" />
        <ScanBox color="#D2E7FF" />
        <ScanBox color="#B9DAFF" />
        <ScanBox color="#8CC2FF" />
      </View>
      <View style={styles.discription}>
        <Text style={{ fontSize: rf(15), textAlign: "center" }}>
          <Text style={{ color: "#007AFF" }}> We recommend you</Text> to
          schedule a {"\n"}consultation with our doctor to get more{"\n"}
          detailed results and recommendations.
        </Text>
      </View>
      {/* Buttons */}
      <ActiveBtn onclick={() => onActivate()} />
      <MainMenuBtn />
      {/* Buttons */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  result: {
    width: wp("100%"),
    height: hp("15%"),
    justifyContent: "center",
    alignItems: "center",
  },
  AnalisisRow: {
    width: wp("100%"),
    height: hp("6%"),
    alignItems: "flex-end",
    justifyContent: "center",
  },
  InnerAnalisis: {
    width: "95%",
    height: "60%",
    borderBottomWidth: 1,
    borderBottomColor: "#e5e5e5",
  },
  ScanBoxRow: {
    width: wp("90%"),
    height: hp("10%"),
    flexDirection: "row",
    alignItems: "center",
  },
  discription: {
    width: wp("100%"),
    height: hp("15%"),
    justifyContent: "center",
    marginBottom: 10,
  },
});
