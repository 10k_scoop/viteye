import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default function Header() {
  return (
    <View style={styles.container}>
      <View style={styles.emptyView}></View>
      <Text style={{ fontSize: rf(18), fontWeight: "700" }}>Result</Text>
      <TouchableOpacity>
        <Text style={{ fontSize: rf(16), color: "#007AFF" }}>New scan</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("12%"),
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "space-between",
    paddingHorizontal: wp("3%"),
  },
  emptyView: {
    width: "20%",
  },
});
