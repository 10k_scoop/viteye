import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity, Modal } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default function ActiveBtn(props) {
  return (
    <View>
      <TouchableOpacity style={styles.container} onPress={props.onclick}>
        <Text style={{ fontSize: rf(14), color: "#fff", fontWeight: "700" }}>
          Activate consultation
        </Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("90%"),
    height: hp("7%"),
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#007AFF",
    marginBottom: 10,
  },
});
