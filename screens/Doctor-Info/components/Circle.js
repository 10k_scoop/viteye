import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default function Circle(props) {
  return (
    <TouchableOpacity style={styles.container}>
      <Text style={{ fontSize: rf(12) }}>{props.txt}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: hp("7%"),
    height: hp("7%"),
    borderRadius: 100,
    backgroundColor: "#B9DAFF",
    justifyContent: "center",
    alignItems: "center",
    marginRight: "3%",
    marginLeft: "3%",
  },
});
