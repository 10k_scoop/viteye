import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { AntDesign } from "@expo/vector-icons";

export default function Header(props) {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={props.onclick}>
        <AntDesign name="left" size={rf(20)} color="#007AFF" />
      </TouchableOpacity>
      <Text style={{ fontSize: rf(15), color: "#007AFF" }}>Chat</Text>
      <View style={styles.title}>
        <Text style={{ fontSize: rf(16), fontWeight: "700" }}>Doctor info</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("10%"),
    backgroundColor: "#F9F9F9",
    flexDirection: "row",
    alignItems: "flex-end",
    paddingVertical: 10,
    paddingHorizontal: 5,
  },
  title: {
    width: "80%",
    height: "40%",
    alignItems: "center",
    justifyContent: "flex-end",
  },
});
