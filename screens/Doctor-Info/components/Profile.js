import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default function Profile(props) {
  return (
    <View style={styles.container}>
      <View style={styles.img}>
        <Image
          source={require("../../../assets/InfoPic.jpg")}
          style={{ width: "100%", height: "100%" }}
        />
      </View>
      <Text style={{ fontSize: rf(20), fontWeight: "700" }}>Shawn Howard</Text>
      <Text style={{ fontSize: rf(16) }}>Doctor's degree</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("30%"),
    alignItems: "center",
    justifyContent: "center",
  },
  img: {
    width: hp("14%"),
    height: hp("14%"),
    borderRadius: 100,
    marginBottom: 10,
    overflow: "hidden",
  },
});
