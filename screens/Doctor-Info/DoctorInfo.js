import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "./components/Header";
import Profile from "./components/Profile";
import Circle from "./components/Circle";

export default function DoctorInfo({ navigation }) {
  return (
    <View style={styles.container}>
      <Header onclick={() => navigation.goBack()} />
      <Profile />
      <View style={styles.CircleRow}>
        <Circle txt="social" />
        <Circle txt="social" />
        <Circle txt="email" />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  CircleRow: {
    width: wp("90%"),
    height: hp("15%"),
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
});
