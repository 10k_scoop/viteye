import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Keyboard,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "./components/Header";
import Reciever from "./components/Reciever";
import Sender from "./components/Sender";
import KeyBoard from "./components/KeyBoard";

export default function Chat({ navigation }) {
  return (
    <View style={styles.container}>
      <Header onclick={() => navigation.goBack()} />
      <View style={styles.title}>
        <Text style={{ fontSize: rf(16), color: "grey" }}>Doctor:</Text>
        <TouchableOpacity
          style={{ marginLeft: 5 }}
          onPress={() => navigation.navigate("DoctorInfo")}
        >
          <Text style={{ fontSize: rf(16), color: "#007AFF" }}>
            Bill Anderson
          </Text>
        </TouchableOpacity>
      </View>
      <Reciever msg="Hello" time="Delivered" />
      <Sender time="Raise to listen play" />
      <KeyBoard />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  title: {
    width: wp("100%"),
    height: hp("8%"),
    paddingHorizontal: wp("5%"),
    borderBottomWidth: 1,
    borderBottomColor: "#e5e5e5",
    flexDirection: "row",
    alignItems: "center",
  },
});
