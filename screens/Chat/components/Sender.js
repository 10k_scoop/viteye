import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { Entypo } from "@expo/vector-icons";

export default function Sender(props) {
  return (
    <View style={styles.container}>
      <View style={styles.msg}>
        <TouchableOpacity>
          <Entypo name="controller-play" size={rf(20)} color="grey" />
        </TouchableOpacity>
        <View style={styles.audio}></View>
      </View>
      <Text style={{ color: "grey", fontSize: rf(12) }}>{props.time}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    justifyContent: "center",
    paddingHorizontal: 10,
    marginTop: 10,
  },
  msg: {
    maxWidth: "70%",
    backgroundColor: "#D1D1D6",
    borderRadius: 10,
    padding: 10,
    flexDirection: "row",
    alignItems: "center",
  },
  audio: {
    width: "80%",
    height: "5%",
    backgroundColor: "grey",
  },
});
