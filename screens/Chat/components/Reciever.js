import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { AntDesign } from "@expo/vector-icons";

export default function Reciever(props) {
  return (
    <View style={styles.container}>
      <View style={styles.msg}>
        <Text style={{ color: "#fff" }}>{props.msg}</Text>
      </View>
      <Text style={{ color: "grey", fontSize: rf(12) }}>{props.time}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    justifyContent: "center",
    alignItems: "flex-end",
    paddingHorizontal: 10,
    marginTop: 10,
  },
  msg: {
    maxWidth: "70%",
    backgroundColor: "#007AFF",
    borderRadius: 10,
    padding: 10,
  },
});
