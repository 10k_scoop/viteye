import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { Entypo, MaterialIcons } from "@expo/vector-icons";

export default function KeyBoard(props) {
  return (
    <View style={styles.container}>
      <Entypo name="camera" size={rf(26)} color="grey" />
      <View style={styles.TextField}>
        <TextInput style={styles.Field} />
        <TouchableOpacity style={styles.Icon}>
          <MaterialIcons name="multitrack-audio" size={rf(20)} color="#fff" />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("10%"),
    position: "absolute",
    bottom: 0,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
    paddingHorizontal: 5,
  },
  TextField: {
    width: "90%",
    height: "50%",
    borderWidth: 0.8,
    borderColor: "#e5e5e5",
    borderRadius: 100,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
  },
  Field: {
    width: "85%",
    height: "100%",
    fontSize: rf(14),
    paddingHorizontal: 5,
  },
  Icon: {
    width: hp("4%"),
    height: hp("4%"),
    borderRadius: 100,
    backgroundColor: "grey",
    justifyContent: "center",
    alignItems: "center",
  },
});
