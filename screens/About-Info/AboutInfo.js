import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import BottomMenu from "../../components/BottomMenu";
import { AntDesign } from "@expo/vector-icons";
import { connect } from "react-redux";
import { logout } from "../../state-management/actions/auth/FirebaseAuthActions";

const AboutInfo = (props) => {
  return (
    <View style={styles.container}>
      <View style={styles.BackIcon}>
        <TouchableOpacity onPress={() => props.navigation.goBack()}>
          <AntDesign name="left" size={rf(22)} color="#007AFF" />
        </TouchableOpacity>
      </View>
      <View style={styles.title}>
        <Text style={{ fontSize: rf(24), fontWeight: "700" }}>More</Text>
      </View>
      <View style={styles.Accounttitle}>
        <Text style={{ fontSize: rf(16), color: "grey" }}>Account</Text>
      </View>
      <View style={styles.AccountDetails}>
        <TouchableOpacity
          style={styles.PersonalData}
          onPress={() =>
            props.navigation.navigate("PersonalData", {
              data: props.get_user_details,
            })
          }
        >
          <Text style={{ fontSize: rf(16) }}>Personal data</Text>
          <View style={styles.Icon}>
            <AntDesign name="right" size={rf(18)} color="grey" />
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.Subscription}
          onPress={() => props.navigation.navigate("Subscription")}
        >
          <Text style={{ fontSize: rf(16) }}>Subscription</Text>
          <View style={styles.Icon}>
            <AntDesign name="right" size={rf(18)} color="grey" />
          </View>
        </TouchableOpacity>
      </View>
      <View style={styles.Infotitle}>
        <Text style={{ fontSize: rf(16), color: "grey" }}>INFO</Text>
      </View>
      <TouchableOpacity
        style={styles.AboutRow}
        onPress={() => props.navigation.navigate("About")}
      >
        <Text style={{ fontSize: rf(16) }}>About</Text>
        <View onPress={() => props.navigation.navigate("About")}>
          <AntDesign name="right" size={rf(18)} color="grey" />
        </View>
      </TouchableOpacity>
      <TouchableOpacity style={styles.logout} onPress={() => props.logout()}>
        <Text style={{ fontSize: rf(16), color: "red" }}>Logout</Text>
      </TouchableOpacity>
      <BottomMenu navigation={props.navigation} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  BackIcon: {
    width: wp("100%"),
    height: hp("10%"),
    justifyContent: "flex-end",
    paddingHorizontal: 5,
  },
  title: {
    width: wp("90%"),
    height: hp("8%"),
    justifyContent: "center",
  },
  Accounttitle: {
    width: wp("100%"),
    height: hp("8%"),
    backgroundColor: "#F2F2F7",
    justifyContent: "flex-end",
    paddingHorizontal: wp("5%"),
    paddingVertical: 8,
  },
  AccountDetails: {
    width: wp("100%"),
    height: hp("18%"),
    alignItems: "flex-end",
  },
  PersonalData: {
    width: "95%",
    height: "50%",
    borderBottomWidth: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    borderBottomColor: "#e5e5e5",
  },
  Subscription: {
    width: "95%",
    height: "50%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  Icon: {
    marginRight: 10,
  },
  Infotitle: {
    width: wp("100%"),
    height: hp("8%"),
    backgroundColor: "#F2F2F7",
    justifyContent: "flex-end",
    paddingHorizontal: wp("5%"),
    paddingVertical: 8,
  },
  AboutRow: {
    width: wp("97%"),
    height: hp("10%"),
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: 10,
    alignItems: "center",
  },
  logout: {
    width: "100%",
    paddingHorizontal: wp("4%"),
  },
  logoutText: {
    color: "red",
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_user_details: state.main.get_user_details,
});
export default connect(mapStateToProps, { logout })(AboutInfo);
