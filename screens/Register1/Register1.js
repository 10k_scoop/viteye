import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  ScrollView,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { AntDesign, Ionicons } from "@expo/vector-icons";

export default function Register1({ navigation }) {
  const [showpassword, setShowPassword] = useState(true);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <AntDesign name="left" size={rf(20)} color="#007AFF" />
        </TouchableOpacity>
      </View>
      <ScrollView>
        <View style={{ width: wp("100%"), alignItems: "center" }}>
          <View style={styles.title}>
            <Text style={{ fontSize: rf(20) }}>Basic data</Text>
          </View>
          <TextInput
            style={styles.TextField}
            placeholder="Name"
            placeholderTextColor="#222"
            onChangeText={(val) => setName(val)}
          />
          <TextInput
            style={styles.TextField}
            placeholder="example@gmail.com"
            placeholderTextColor="#222"
            onChangeText={(val) => setEmail(val)}
          />
          <View style={styles.PasswordRow}>
            <TextInput
              style={styles.Password}
              placeholder="************"
              placeholderTextColor="#222"
              secureTextEntry={showpassword}
              onChangeText={(val) => setPass(val)}
            />
            <TouchableOpacity onPress={() => setShowPassword(!showpassword)}>
              <Ionicons name="ios-eye-outline" size={rf(18)} color="grey" />
            </TouchableOpacity>
          </View>
          <View style={styles.BtnRow}>
            <TouchableOpacity
              style={styles.Btn}
              onPress={() => {
                if (email == "" || name == "" || pass == "") {
                  alert("fill all details");
                } else {
                  if (pass >= 8) {
                    navigation.navigate("Register2", {
                      data: { name: name, email: email, pass: pass },
                    });
                  } else {
                    alert("Password should be at least 8 characters");
                  }
                }
              }}
            >
              <Text
                style={{ fontSize: rf(16), color: "#fff", fontWeight: "700" }}
              >
                Next
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  header: {
    width: wp("100%"),
    height: hp("11%"),
    justifyContent: "flex-end",
    paddingHorizontal: "2%",
  },
  title: {
    width: wp("100%"),
    height: hp("28%"),
    alignItems: "center",
    justifyContent: "flex-end",
    marginBottom: "10%",
  },
  TextField: {
    width: wp("90%"),
    height: hp("7%"),
    borderBottomWidth: 1.5,
    fontSize: rf(15),
    borderBottomColor: "#e5e5e5",
    marginBottom: "3%",
  },
  PasswordRow: {
    width: wp("90%"),
    height: hp("7%"),
    borderBottomWidth: 1.5,
    flexDirection: "row",
    alignItems: "center",
    borderBottomColor: "#e5e5e5",
  },
  Password: {
    width: "90%",
    height: "80%",
    fontSize: rf(15),
  },
  Forget: {
    width: wp("90%"),
    height: hp("9%"),
    justifyContent: "center",
  },
  BtnRow: {
    width: wp("100%"),
    height: hp("25%"),
    alignItems: "center",
    justifyContent: "flex-end",
  },
  Btn: {
    width: wp("90%"),
    height: hp("7%"),
    backgroundColor: "#007AFF",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
  },
});
