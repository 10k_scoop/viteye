import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { AntDesign } from "@expo/vector-icons";

export default function OnBoarding3({ navigation }) {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <AntDesign name="left" size={rf(20)} color="#007AFF" />
        </TouchableOpacity>
        <TouchableOpacity>
          <Text style={{ fontSize: rf(16), color: "#007AFF" }}>Skip</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.CircleRow}>
        <View style={styles.Circle}></View>
      </View>
      <View style={styles.Discription}>
        <Text style={styles.Font}>
          Text 3 access to 100+{"\n"} ad-free games.{"\n"} No in-app purchases.
        </Text>
      </View>
      <View style={styles.DotsRow}>
        <View style={styles.DotRapper}>
          <View style={styles.Dot1}></View>
          <View style={styles.Dot2}></View>
          <View style={styles.Dot3}></View>
        </View>
      </View>
      <TouchableOpacity
        style={styles.Btn}
        onPress={() => navigation.navigate("GetStarted")}
      >
        <Text style={{ fontSize: rf(16), color: "#fff", fontWeight: "700" }}>
          Next
        </Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  header: {
    width: wp("100%"),
    height: hp("11%"),
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "space-between",
    paddingHorizontal: "3%",
    paddingVertical: "2%",
  },
  CircleRow: {
    width: wp("100%"),
    height: hp("45%"),
    justifyContent: "center",
    alignItems: "center",
  },
  Circle: {
    width: wp("65%"),
    height: wp("65%"),
    borderRadius: wp("65%"),
    backgroundColor: "#DDEDFF",
  },
  Discription: {
    width: wp("100%"),
    height: hp("15%"),
    alignItems: "center",
  },
  Font: {
    fontSize: rf(24),
    fontWeight: "700",
    textAlign: "center",
  },
  DotsRow: {
    width: wp("100%"),
    height: hp("12%"),
    alignItems: "center",
    justifyContent: "center",
  },
  Dot1: {
    width: hp("1%"),
    height: hp("1%"),
    backgroundColor: "#e5e5e5",
    borderRadius: 100,
  },
  Dot2: {
    width: hp("1%"),
    height: hp("1%"),
    backgroundColor: "#e5e5e5",
    borderRadius: 100,
  },
  Dot3: {
    width: hp("1%"),
    height: hp("1%"),
    backgroundColor: "#007AFF",
    borderRadius: 100,
  },
  DotRapper: {
    width: "12%",
    height: "30%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
  },
  Btn: {
    width: wp("90%"),
    height: hp("7%"),
    backgroundColor: "#007AFF",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
  },
});
