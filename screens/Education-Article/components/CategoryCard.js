import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default function CategoryCard(props) {
  return (
    <TouchableOpacity style={styles.container} onPress={props.onClick}>
      <Text style={{ fontSize: rf(12), color: "#fff" }}>CATEGORY</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("23%"),
    height: hp("4%"),
    borderRadius: 100,
    backgroundColor: "#007AFF",
    alignItems: "center",
    justifyContent: "center",
    marginLeft: wp("3%"),
    marginRight: wp("1%"),
  },
});
