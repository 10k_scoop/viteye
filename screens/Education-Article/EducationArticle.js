import React, { useState } from "react";
import { StyleSheet, Text, View, ScrollView } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "./components/Header";
import CategoryCard from "./components/CategoryCard";
import ArticleCards from "./components/ArticleCards";
import BottomMenu from "../../components/BottomMenu";

export default function EducationArticle({ navigation }) {
  return (
    <View style={styles.container}>
      <Header />
      <ScrollView>
        <View style={styles.Wrapper}>
          <View style={styles.CategoryRow}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            >
              <CategoryCard onClick={() => navigation.navigate("Articles")} />
              <CategoryCard />
              <CategoryCard />
              <CategoryCard />
              <CategoryCard />
              <CategoryCard />
              <CategoryCard />
              <CategoryCard />
            </ScrollView>
          </View>
          <View style={styles.Firsttitle}>
            <Text style={{ fontSize: rf(18), fontWeight: "700" }}>
              Articles section title
            </Text>
          </View>
          {/* ArticleCards */}
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            <View style={styles.ArticleCardsWrapper}>
              <ArticleCards />
              <ArticleCards />
              <ArticleCards />
              <ArticleCards />
              <ArticleCards />
              <ArticleCards />
            </View>
          </ScrollView>
          {/* ArticleCards */}
          <View style={styles.Secondtitle}>
            <Text style={{ fontSize: rf(18), fontWeight: "700" }}>
              Articles section title
            </Text>
          </View>
          {/* ArticleCards */}
          <ScrollView horizontal showsHorizontalScrollIndicator={false}>
            <View style={styles.ArticleCardsWrapper}>
              <ArticleCards />
              <ArticleCards />
              <ArticleCards />
              <ArticleCards />
              <ArticleCards />
              <ArticleCards />
            </View>
          </ScrollView>
          {/* ArticleCards */}
        </View>
      </ScrollView>
      <BottomMenu navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Wrapper: {
    width: wp("100%"),
    alignItems: "center",
    marginBottom: hp("10%"),
  },
  CategoryRow: {
    height: hp("13%"),
    flexDirection: "row",
    alignItems: "center",
  },
  Firsttitle: {
    width: wp("95%"),
    height: hp("6%"),
    justifyContent: "center",
  },
  ArticleCardsWrapper: {
    flexDirection: "row",
    marginRight: 10,
  },
  Secondtitle: {
    width: wp("95%"),
    height: hp("10%"),
    justifyContent: "center",
  },
});
