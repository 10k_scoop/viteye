import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  ActivityIndicator,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { AntDesign, Ionicons } from "@expo/vector-icons";
import { signup } from "../../state-management/actions/auth/FirebaseAuthActions";
import { connect } from "react-redux";

const Register2 = (props) => {
  const [fullName, setFullName] = useState("");
  const [loading, setLoading] = useState(false);

  let data = props.route.params.data;

  const onSignup = () => {
    if (fullName != "") {
      let newData = {
        name: data.name,
        email: data.email,
        password: data.pass,
        fullName: fullName,
        profile:
          "https://avatoon.me/wp-content/uploads/2021/09/Cartoon-Pic-Ideas-for-DP-Profile08.png",
      };
      setLoading(true);
      props.signup(newData, setLoading);
    } else {
      alert("Enter your full name");
    }
  };

  if (loading) {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <ActivityIndicator size="large" color="#222" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity onPress={() => props.navigation.goBack()}>
          <AntDesign name="left" size={rf(20)} color="#007AFF" />
        </TouchableOpacity>
      </View>
      <View style={styles.title}>
        <Text style={{ fontSize: rf(20) }}>Your Full Name</Text>
      </View>
      <View style={styles.TextFieldRow}>
        <TextInput
          style={styles.textField}
          placeholder="Enter here"
          placeholderTextColor="grey"
          onChangeText={(val) => setFullName(val)}
        />
      </View>
      <View style={styles.BtnRow}>
        <TouchableOpacity style={styles.Btn} onPress={() => onSignup()}>
          <Text style={{ fontSize: rf(16), color: "#fff", fontWeight: "700" }}>
            Next
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  header: {
    width: wp("100%"),
    height: hp("11%"),
    justifyContent: "flex-end",
    paddingHorizontal: "2%",
  },
  title: {
    width: wp("100%"),
    height: hp("28%"),
    alignItems: "center",
    justifyContent: "flex-end",
    marginBottom: "10%",
  },
  TextFieldRow: {
    width: wp("100%"),
    height: hp("8%"),
    justifyContent: "center",
    alignItems: "center",
  },
  textField: {
    width: "32%",
    height: "100%",
    fontSize: rf(26),
    alignItems: "center",
    paddingHorizontal: "1%",
  },
  BtnRow: {
    width: wp("100%"),
    height: hp("40%"),
    alignItems: "center",
    justifyContent: "flex-end",
  },

  Btn: {
    width: wp("90%"),
    height: hp("7%"),
    backgroundColor: "#007AFF",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  signup_details: state.main.signup_details,
});
export default connect(mapStateToProps, { signup })(Register2);
