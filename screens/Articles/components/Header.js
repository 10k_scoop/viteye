import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default function Header({ onPress }) {
  return (
    <View style={styles.container}>
      <View style={styles.BtnRow}>
        <TouchableOpacity style={styles.CategoryBtn}>
          <Text style={{ fontSize: rf(12), color: "#007AFF" }}>Category</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.title}>
        <Text style={{ fontSize: rf(24), fontWeight: "700" }}>
          Articles {"\n"}section title
        </Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("25%"),
    backgroundColor: "#DDEDFF",
  },
  BtnRow: {
    width: "100%",
    height: "45%",
    justifyContent: "center",
    paddingHorizontal: wp("5%"),
  },
  CategoryBtn: {
    width: "22%",
    height: "35%",
    borderWidth: 1,
    borderRadius: 100,
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#007AFF",
  },
  title: {
    width: "100%",
    height: "40%",
    paddingHorizontal: wp("5%"),
  },
});
