import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "./components/Header";
import { AntDesign } from "@expo/vector-icons";

export default function Articles({ navigation }) {
  return (
    <View style={styles.container}>
      {/* IconRow */}
      <View style={styles.IconRow}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <AntDesign name="left" size={rf(22)} color="#007AFF" />
        </TouchableOpacity>
      </View>
      {/* IconRow */}
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{ width: wp("100%"), alignItems: "center" }}>
          {/* Header */}
          <Header />
          {/* Header */}
          <View style={styles.title}>
            <Text style={{ fontSize: rf(20), fontWeight: "600" }}>heading</Text>
          </View>
          <View style={styles.discription}>
            <Text style={{ fontSize: rf(15) }}>
              Lorem ipsum dolor sit amet, consectetur adipisci- ng elit. Sed
              praesent purus nibh quam malesuada auctor vestibulum ut. Dolor
              etiam pellentesque netus quis. Et aliquet non arcu velit
              pellentesque fermentum cursus tellus praesent. Dui odio ege- stas
              est faucibus sed.
            </Text>
          </View>
          <View style={styles.discription}>
            <Text style={{ fontSize: rf(15) }}>
              Lorem ipsum dolor sit amet, consectetur adipisci- ng elit. Sed
              praesent purus nibh quam malesuada auctor vestibulum ut. Dolor
              etiam pellentesque netus quis. Et aliquet non arcu velit
              pellentesque fermentum cursus tellus praesent. Dui odio ege- stas
              est faucibus sed.
            </Text>
          </View>
          <View style={styles.Box}></View>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  IconRow: {
    width: wp("100%"),
    height: hp("10%"),
    justifyContent: "flex-end",
    paddingHorizontal: 5,
    backgroundColor: "#DDEDFF",
  },
  Header: {
    width: wp("100%"),
    height: hp("30%"),
    backgroundColor: "red",
  },
  title: {
    width: wp("90%"),
    height: hp("10%"),
    justifyContent: "center",
  },
  discription: {
    width: wp("90%"),
    height: hp("14%"),
  },
  Box: {
    width: wp("90%"),
    height: hp("35%"),
    backgroundColor: "#DDEDFF",
    marginBottom: 10,
  },
});
