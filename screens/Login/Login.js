import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  ScrollView,
  ActivityIndicator,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { AntDesign, Ionicons } from "@expo/vector-icons";
import { connect } from "react-redux";
import { login } from "../../state-management/actions/auth/FirebaseAuthActions";
const Login = (props) => {
  const [showpassword, setShowPassword] = useState(true);
  const [loading, setLoading] = useState(false);
  const [email, setEmail] = useState("");
  const [pass, setPass] = useState("");

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity onPress={() => props.navigation.goBack()}>
          <AntDesign name="left" size={rf(20)} color="#007AFF" />
        </TouchableOpacity>
      </View>
      <ScrollView>
        <View style={{ width: wp("100%"), alignItems: "center" }}>
          <View style={styles.title}>
            <Text style={{ fontSize: rf(20) }}>Login</Text>
          </View>
          <TextInput
            style={styles.TextField}
            placeholder="example@gmail.com"
            placeholderTextColor="#222"
            onChangeText={(val) => setEmail(val)}
          />
          <View style={styles.PasswordRow}>
            <TextInput
              style={styles.Password}
              placeholder="************"
              placeholderTextColor="#222"
              secureTextEntry={showpassword}
              onChangeText={(val) => setPass(val)}
            />
            <TouchableOpacity onPress={() => setShowPassword(!showpassword)}>
              <Ionicons name="ios-eye-outline" size={rf(18)} color="grey" />
            </TouchableOpacity>
          </View>
          <View style={styles.Forget}>
            <TouchableOpacity>
              <Text style={{ fontSize: rf(14), color: "#007AFF" }}>
                Forget Password?
              </Text>
            </TouchableOpacity>
          </View>

          <TouchableOpacity
            style={styles.Btn}
            onPress={() => {
              loading ? undefined : setLoading(true);
              props.login(email, pass, setLoading);
            }}
          >
            <Text
              style={{ fontSize: rf(16), color: "#fff", fontWeight: "700" }}
            >
              {loading ? (
                <ActivityIndicator size="small" color="#fff" />
              ) : (
                "Login"
              )}
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  header: {
    width: wp("100%"),
    height: hp("11%"),
    justifyContent: "flex-end",
    paddingHorizontal: "2%",
  },
  title: {
    width: wp("100%"),
    height: hp("27%"),
    alignItems: "center",
    justifyContent: "flex-end",
    marginBottom: "10%",
  },
  TextField: {
    width: wp("90%"),
    height: hp("7%"),
    borderBottomWidth: 1.5,
    fontSize: rf(15),
    borderBottomColor: "#e5e5e5",
    marginBottom: "5%",
  },
  PasswordRow: {
    width: wp("90%"),
    height: hp("8%"),
    borderBottomWidth: 1.5,
    flexDirection: "row",
    alignItems: "center",
    borderBottomColor: "#e5e5e5",
  },
  Password: {
    width: "90%",
    height: "80%",
    fontSize: rf(15),
  },
  Forget: {
    width: wp("90%"),
    height: hp("9%"),
    justifyContent: "center",
    marginBottom: "20%",
  },
  Btn: {
    width: wp("90%"),
    height: hp("7%"),
    backgroundColor: "#007AFF",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  login_details: state.main.login_details,
});
export default connect(mapStateToProps, { login })(Login);
