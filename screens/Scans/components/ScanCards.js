import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default function ScanCards(props) {
  return (
    <TouchableOpacity style={styles.container} onPress={props.onclick}>
      <View style={styles.Inner}>
        <Text style={{ fontSize: rf(14), color: "grey" }}>Lilah Grace</Text>
        <Text style={{ fontSize: rf(12), color: props.color1 }}>
          {props.txt3}
        </Text>
      </View>
      <Text style={{ fontSize: rf(16), fontWeight: "700" }}>{props.txt}</Text>
      <Text style={{ fontSize: rf(14), color: props.color }}>{props.txt2}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("95%"),
    height: hp("13%"),
    backgroundColor: "#F2F2F7",
    borderRadius: 10,
    paddingHorizontal: wp("3%"),
    justifyContent: "space-evenly",
    paddingVertical: 5,
    marginBottom: 10,
  },
  Inner: {
    width: "100%",
    height: "30%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
});
