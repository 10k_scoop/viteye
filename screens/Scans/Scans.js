import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import ScanCards from "./components/ScanCards";
import BottomMenu from "../../components/BottomMenu";

export default function Scans({ navigation }) {
  return (
    <View style={styles.container}>
      <View style={styles.Header}>
        <TouchableOpacity>
          <Text style={{ fontSize: rf(16), color: "#007AFF" }}>New scan</Text>
        </TouchableOpacity>
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View
          style={{
            width: wp("100%"),
            alignItems: "center",
            marginBottom: hp("10%"),
          }}
        >
          <View style={styles.title}>
            <Text style={{ fontSize: rf(22), fontWeight: "700" }}>Scans</Text>
          </View>
          <View style={styles.ActiveRow}>
            <Text style={{ fontSize: rf(14), color: "grey" }}>ACTIVE</Text>
          </View>
          <ScanCards
            txt="Scan #34. Right shoulder mole"
            txt2="Melanoma probability 97%"
            txt3="CONSULTATION IS ACTIVE"
            color="#007AFF"
            color1="#007AFF"
            onclick={() => navigation.navigate("ScanResult1")}
          />
          <ScanCards
            txt="Scan #35. Right hand top"
            txt2="Melanoma is not detected (4%)"
            txt3="CONSULTATION IS NOT ACTIVE"
            color="grey"
            color1="grey"
          />

          <View style={styles.ActiveRow}>
            <Text style={{ fontSize: rf(14), color: "grey" }}>UNACTIVE</Text>
          </View>
          <ScanCards
            txt="Scan #34. Right shoulder mole"
            txt2="Melanoma probability 97%"
            txt3="CONSULTATION IS ACTIVE"
            color="#007AFF"
            color1="#007AFF"
          />
          <ScanCards
            txt="Scan #35. Right hand top"
            txt2="Melanoma is not detected (4%)"
            txt3="CONSULTATION IS NOT ACTIVE"
            color="grey"
            color1="grey"
          />
        </View>
      </ScrollView>
      <BottomMenu navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  Header: {
    width: wp("100%"),
    height: hp("10%"),
    justifyContent: "flex-end",
    alignItems: "flex-end",
    paddingHorizontal: 10,
  },
  title: {
    width: wp("95%"),
    height: hp("8%"),
    justifyContent: "center",
  },
  ActiveRow: {
    width: wp("95%"),
    height: hp("6%"),
    justifyContent: "center",
  },
});
