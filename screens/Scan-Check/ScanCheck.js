import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default function ScanCheck({ navigation }) {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={{ fontSize: rf(18), fontWeight: "700" }}>
          Check Scan 1
        </Text>
      </View>
      <View style={styles.Img}>
        <ImageBackground
          style={{ width: "100%", height: "100%" }}
          source={require("../../assets/Example.png")}
        ></ImageBackground>
      </View>
      <TouchableOpacity
        style={styles.ContinueBtn}
        onPress={() => navigation.navigate("Consultation")}
      >
        <Text style={{ fontSize: rf(16), color: "#fff" }}>Continue</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.RescanBtn}>
        <Text style={{ fontSize: rf(16), color: "#222" }}>Rescan</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  header: {
    width: wp("100%"),
    height: hp("10%"),
    justifyContent: "flex-end",
    alignItems: "center",
  },
  Img: {
    width: wp("100%"),
    height: hp("60%"),
    marginTop: "10%",
    marginBottom: hp("5%"),
  },
  discription: {
    width: "100%",
    height: "20%",
    justifyContent: "center",
    alignItems: "center",
  },
  WarningScan: {
    width: "100%",
    height: "70%",
    justifyContent: "flex-end",
    alignItems: "center",
  },
  ContinueBtn: {
    width: wp("90%"),
    height: hp("7%"),
    backgroundColor: "#007AFF",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 8,
  },
  RescanBtn: {
    width: wp("90%"),
    height: hp("7%"),
    backgroundColor: "#F4F4F5",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
  },
});
