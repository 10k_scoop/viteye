import React, { useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default function TeamCards(props) {
  return (
    <View style={styles.container}>
      <View style={styles.Circle}></View>
      <Text style={{ fontSize: rf(16), fontWeight: "600" }}>
        Team Team{"\n"}
        <Text style={{ fontSize: rf(14), color: "grey" }}>Role</Text>
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("45%"),
    height: hp("23%"),
    justifyContent: "space-evenly",
  },
  Circle: {
    width: hp("15%"),
    height: hp("15%"),
    backgroundColor: "#DDEDFF",
    borderRadius: 100,
  },
});
