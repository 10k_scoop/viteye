import React, { useState } from "react";
import { StyleSheet, Text, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default function ContactBtn(props) {
  return (
    <TouchableOpacity style={styles.container}>
      <Text style={{ fontSize: rf(16), fontWeight: "700", color: "#fff" }}>
        Conatct us
      </Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("90%"),
    height: hp("7%"),
    borderRadius: 10,
    backgroundColor: "#007AFF",
    justifyContent: "center",
    alignItems: "center",
    marginTop: hp("5%"),
  },
});
