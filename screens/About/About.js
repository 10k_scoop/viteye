import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "./components/Header";
import TeamCards from "./components/TeamCards";
import ContactBtn from "./components/ContactBtn";

export default function About({ navigation }) {
  return (
    <View style={styles.container}>
      <Header onClick={() => navigation.goBack()} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Wrapper}>
          <View style={styles.CircleRow}>
            <View style={styles.Circle}></View>
          </View>
          <View style={styles.title}>
            <Text style={{ fontSize: rf(20), fontWeight: "600" }}>heading</Text>
          </View>
          <View style={styles.discription}>
            <Text style={{ fontSize: rf(15) }}>
              Lorem ipsum dolor sit amet, consectetur adipisci- ng elit. Sed
              praesent purus nibh quam malesuada auctor vestibulum ut. Dolor
              etiam pellentesque netus quis. Et aliquet non arcu velit
              pellentesque fermentum cursus tellus praesent. Dui odio ege- stas
              est faucibus sed.
            </Text>
          </View>
          <View style={styles.title}>
            <Text style={{ fontSize: rf(20), fontWeight: "600" }}>
              Why we do this
            </Text>
          </View>
          <View style={styles.discription}>
            <Text style={{ fontSize: rf(15) }}>
              Lorem ipsum dolor sit amet, consectetur adipisci- ng elit. Sed
              praesent purus nibh quam malesuada auctor vestibulum ut. Dolor
              etiam pellentesque netus quis. Et aliquet non arcu velit
              pellentesque fermentum cursus tellus praesent. Dui odio ege- stas
              est faucibus sed.
            </Text>
          </View>
          <View style={styles.discription}>
            <Text style={{ fontSize: rf(15) }}>
              Lorem ipsum dolor sit amet, consectetur adipisci- ng elit. Sed
              praesent purus nibh quam malesuada auctor vestibulum ut. Dolor
              etiam pellentesque netus quis. Et aliquet non arcu velit
              pellentesque fermentum cursus tellus praesent. Dui odio ege- stas
              est faucibus sed.
            </Text>
          </View>
          <View style={styles.Teamheading}>
            <Text style={{ fontSize: rf(20), fontWeight: "600" }}>Team</Text>
          </View>
          {/* components */}
          <View style={styles.TeamRow}>
            <TeamCards />
            <TeamCards />
          </View>
          {/* components */}
          <View style={styles.TeamRow}>
            <TeamCards />
            <TeamCards />
          </View>
          {/* components */}
          <ContactBtn />
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  CircleRow: {
    width: wp("100%"),
    height: hp("35%"),
    justifyContent: "flex-end",
    alignItems: "center",
  },
  Circle: {
    width: hp("30%"),
    height: hp("30%"),
    borderRadius: 100,
    backgroundColor: "#DDEDFF",
  },
  title: {
    width: wp("90%"),
    height: hp("10%"),
    justifyContent: "center",
  },
  discription: {
    width: wp("90%"),
    height: hp("12%"),
  },
  Wrapper: {
    width: wp("100%"),
    alignItems: "center",
    marginBottom: hp("5%"),
  },
  Teamheading: {
    width: wp("90%"),
    height: hp("10%"),
    justifyContent: "center",
  },
  TeamRow: {
    width: wp("90%"),
    height: hp("25%"),
    flexDirection: "row",
    justifyContent: "space-around",
  },
});
