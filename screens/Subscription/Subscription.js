import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "./components/Header";
import Radio from "./components/Radio";
import MonthlyButton from "./components/MonthlyButton";
import YearButton from "./components/YearButton";

export default function Subscription({ navigation }) {
  const [checked, setChecked] = useState("1");
  return (
    <View style={styles.container}>
      <Header onPress={() => navigation.goBack()} />
      <View style={styles.CircleRow}>
        <View style={styles.Circle}></View>
      </View>
      <View style={styles.Discription}>
        <Text style={{ fontSize: rf(22), fontWeight: "700", marginBottom: 7 }}>
          Subscribe to full access
        </Text>
        <Text style={{ fontSize: rf(16) }}>
          Make shopping smarter, avoid extra trips to {"\n"}
          the supermarkets & malls and get more{"\n"}organized using
          Shopifylist.
        </Text>
      </View>
      {/* RadioButton */}
      <View style={styles.RadioButtonRow}>
        <Radio
          title="Make Shopping smarter"
          id="1"
          onPress={() => setChecked("1")}
          checked={checked}
        />
        <Radio
          title="Make Shopping smarter"
          id="2"
          onPress={() => setChecked("2")}
          checked={checked}
        />
        <Radio
          title="Make Shopping smarter"
          id="3"
          onPress={() => setChecked("3")}
          checked={checked}
        />
      </View>
      {/* Buttons */}
      <MonthlyButton onPress={() => navigation.navigate("Subscription1")} />
      <YearButton />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  CircleRow: {
    width: wp("100%"),
    height: hp("30%"),
    justifyContent: "center",
    alignItems: "center",
  },
  Circle: {
    width: hp("25%"),
    height: hp("25%"),
    borderRadius: 100,
    backgroundColor: "#DDEDFF",
  },
  Discription: {
    width: wp("80%"),
    height: hp("15%"),
    paddingHorizontal: "5%",
  },
  RadioButtonRow: {
    width: wp("80%"),
    height: hp("18%"),
    marginBottom: "8%",
  },
});
