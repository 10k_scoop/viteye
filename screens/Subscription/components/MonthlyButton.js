import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default function MonthlyButton(props) {
  return (
    <TouchableOpacity style={styles.container} onPress={props.onPress}>
      <Text style={styles.Font1}>
        Subscribe monthly{"\n"}
        <Text style={styles.Font2}> $3.99/month</Text>
      </Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("90%"),
    height: hp("7%"),
    backgroundColor: "#E9E9EB",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: "3%",
  },
  Font1: {
    fontSize: rf(16),
    color: "#007AFF",
    textAlign: "center",
    fontWeight: "700",
  },
  Font2: {
    fontSize: rf(14),
    fontWeight: "100",
  },
});
