import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default function YearButton({ navigation, onPress }) {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <Text style={styles.Font1}>
        Subscribe for year{"\n"}
        <Text style={styles.Font2}> $50/year</Text>
      </Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("90%"),
    height: hp("7%"),
    backgroundColor: "#007AFF",
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
  },
  Font1: {
    fontSize: rf(16),
    color: "#fff",
    textAlign: "center",
    fontWeight: "700",
  },
  Font2: {
    fontSize: rf(14),
    fontWeight: "100",
  },
});
