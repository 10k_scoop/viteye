import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { AntDesign } from "@expo/vector-icons";

export default function Header({ onPress }) {
  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={onPress}>
        <AntDesign name="left" size={rf(20)} color="#007AFF" />
      </TouchableOpacity>
      <Text style={{ fontSize: rf(18), fontWeight: "700" }}>Subscription</Text>
      <TouchableOpacity>
        <Text style={{ fontSize: rf(16), color: "#007AFF" }}>Later</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("12%"),
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-end",
    paddingVertical: "2%",
    paddingHorizontal: "3%",
  },
});
