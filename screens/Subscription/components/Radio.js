import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { heightPercentageToDP as hp, widthPercentageToDP as wp, } from "react-native-responsive-screen";
import { RadioButton } from 'react-native-paper';

export default function Radio(props) {
    
    return (
        <View style={styles.container}>
            <View>
                <RadioButton
                    value={props.checked}
                    status={props.checked === props.id ? 'checked' : 'unchecked'}
                    onPress={props.onPress}
                />
            </View>
            <Text style={{ fontSize: rf(16) }}>{props.title}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        width: "70%",
        height: "30%",
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
        paddingHorizontal: "3%"
    },
    Btn: {
        width: hp('3%'),
        height: hp('3%'),
        borderRadius: 100,
        backgroundColor: "#C4C4C4"
    }


});
