import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "./components/Header";

export default function ScanMole({ navigation }) {
  return (
    <View style={styles.container}>
      <Header onPress={() => navigation.goBack()} />
      <View style={styles.heading}>
        <Text
          style={{ fontSize: rf(22), fontWeight: "700", textAlign: "center" }}
        >
          Pin up dermatoscope on {"\n"}your phone
        </Text>
      </View>
      <View style={styles.discription}>
        <Text style={{ fontSize: rf(16), textAlign: "center" }}>
          With a goal, Health can recommend a{"\n"} bedtime and wake-up alarm
          and will also{"\n"} keep track of your time in bed.
        </Text>
      </View>
      <View style={styles.ImgRow}>
        <Image
          source={require("../../assets/ScanImg.png")}
          style={{ width: "100%", height: "100%" }}
          resizeMode="cover"
        />
      </View>
      <TouchableOpacity
        style={styles.Btn}
        onPress={() => navigation.navigate("ScanExample")}
      >
        <Text style={{ fontSize: rf(18), color: "#fff", fontWeight: "700" }}>
          Ok
        </Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
  },
  heading: {
    width: wp("100%"),
    height: hp("15%"),
    alignItems: "center",
    justifyContent: "center",
  },
  discription: {
    width: wp("100%"),
    height: hp("12%"),
    alignItems: "center",
  },
  ImgRow: {
    width: wp("100%"),
    height: hp("50%"),
  },
  Btn: {
    width: wp("90%"),
    height: hp("7%"),
    backgroundColor: "#007AFF",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
  },
});
