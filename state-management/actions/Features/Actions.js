import { GET_ERRORS } from "../../types/types";
import * as firebase from "firebase";

export const updateProfile = (data, id, setLoading) => async (dispatch) => {
  var db = firebase.firestore();
  try {
    if (data.profile) {
      const response = await fetch(data.profile);
      const blob = await response.blob();
      let imageRef = firebase.storage().ref("images/" + id + ".jpg");
      imageRef
        .put(blob)
        .then((snapshot) => {
          //You can check the image is now uploaded in the storage bucket
          imageRef
            .getDownloadURL()
            .then((url) => {
              var doc = db.collection("users").doc(id);
              return doc
                .update({
                  name: data.name,
                  gender: data.gender,
                  age: data.age,
                  profile: url,
                })
                .then(() => {
                  setLoading(false);
                  alert("Profile Updated");
                })
                .catch((e) => {
                  setLoading(false);
                  console.log(e.message);
                });
            })
            .catch((e) =>
              console.log("getting downloadURL of image error => ", e)
            );
        })
        .catch((e) => console.log("uploading image error => ", e));
    } else {
      var doc = db.collection("users").doc(id);
      return doc
        .update({
          name: data.name,
          gender: data.gender,
          age: data.age,
        })
        .then(() => {
          setLoading(false);
          alert("Profile Updated");
        })
        .catch((e) => {
          setLoading(false);
          console.log(e.message);
        });
    }
  } catch (e) {
    setLoading(false);
    console.log(e.message);
  }
};
